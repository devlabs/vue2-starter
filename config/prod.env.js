'use strict'
const merge = require('webpack-merge')
require('dotenv').config();
let _ = require('lodash');
let env = {};

// https://github.com/vuejs-templates/webpack/issues/722#issuecomment-310575195
env = _(process.env)
.pickBy( (value, key) => key.match(/^APP_/))
.mapKeys((value, key) => key.substring('APP_'.length))
.mapValues(value => `"${value}"`)
.value();

module.exports = merge(env, {
  NODE_ENV: '"production"'
})

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueResource from 'vue-resource'
import CredentialsService from '@/services/CredentialsService'

Vue.config.productionTip = false
Vue.use(VueResource)

const credentials = new CredentialsService()

// Add HTTP Interceptor
Vue.http.interceptors.push(function (request, next) {
  request.headers.set('Authorization', credentials.getToken())
  request.headers.set('Accept', 'application/json')

  next(function (response) {
    if (response.status === 401) {
      credentials.clearCredentials()
      // window.location.href = (address.spa)
      this.$router.push('/login')
      this.$dispatch('logout')
    }
  })
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})

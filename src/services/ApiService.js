import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)

const API_URL = process.env.API_URL

class ApiConnect {
  constructor (route) {
    this.resource = Vue.resource(API_URL + route)
    this.baseUrl = API_URL
  }
  getResource () {
    return this.resource
  }
  query () {
    return this.resource.query()
  }
  getById (id) {
    return this.resource.get({id})
  }
  save (model) {
    return this.resource.save(model)
  }
  update (id, model) {
    return this.resource.update({id}, model)
  }
  destroy (id) {
    return this.resource.delete({id})
  }
}

export default ApiConnect

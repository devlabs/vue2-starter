import ApiService from './ApiService'

class UsersService extends ApiService {
  constructor (endpoint) {
    super(endpoint)
    this.resourceName = endpoint
  }
}

export const usersService = new UsersService('users')

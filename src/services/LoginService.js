import ApiService from './ApiService'
import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)

class LoginService extends ApiService {
  constructor (endpoint) {
    super(endpoint)
    this.resourceName = endpoint
  }
  authenticate (credentials) {
    return Vue.http.post(this.baseUrl + this.resourceName, credentials)
  }
}

export const loginService = new LoginService('login')
